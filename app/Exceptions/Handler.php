<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    public function report(Exception $exception)
    {
        \Log::warning($exception->getMessage());
    }

    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return response()->json(['status' => 'validation', 'errors' => [$exception->errors()]], 400);
        }
        if ($exception instanceof ModelNotFoundException) {
            return response()->json(['status' => 'validation', 'errors' => [$exception->getMessage()]], 404);
        }
        return response()->json(['status' => 'error', 'errors' => [$exception->getMessage()]], 500);
    }
}
