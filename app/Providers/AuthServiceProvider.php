<?php

namespace App\Providers;

use App\Auth\AuthorizationInterace;
use App\Auth\AuthService;
use GuzzleHttp\Client;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $app = $this->app;
        $app->bind(AuthorizationInterace::class, function () use ($app) {
            $authServiceUrl = $app['config']['http']['services']['auth']['uri'];
            $guzzle = new Client(['base_uri' => $authServiceUrl]);
            return new AuthService($guzzle);
        });
    }
}
