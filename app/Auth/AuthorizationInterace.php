<?php

namespace App\Auth;

use Illuminate\Http\Request;

interface AuthorizationInterace
{
    public function isAuthenticated(Request $request): bool;
}