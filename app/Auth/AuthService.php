<?php

namespace App\Auth;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AuthService implements AuthorizationInterace
{
    const AUTHORIZATION_HEADER = 'Authorization';
    const USER_ENDPOINT = '/api/user';

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function isAuthenticated(Request $request): bool
    {
        if (!$request->hasHeader(self::AUTHORIZATION_HEADER)) {
            return false;
        }
        if (!$this->isTokenValid($request->header(self::AUTHORIZATION_HEADER))) {
            return false;
        }
        return true;
    }

    private function isTokenValid(string $token): bool
    {
        try {
            $result = $this->httpClient->request(
                'GET',
                self::USER_ENDPOINT,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => $token,
                    ],
                ]
            );
            return $result->getStatusCode() === 200;
        } catch (\Exception $e) {
            \Log::warning($e->getMessage());
            return false;
        }
    }
}