<?php

namespace App\Http\Middleware;

use App\Auth\AuthorizationInterace;
use Closure;
use Illuminate\Http\Request;

class AuthMiddleware
{
    private $authService;

    public function __construct(AuthorizationInterace $authService)
    {
        $this->authService = $authService;
    }

    public function handle(Request $request, Closure $next, $guard = null)
    {
        if ($this->authService->isAuthenticated($request)) {
            return $next($request);
        }

        return response()->json(['status' => 'error', 'errors' => ['Unauthorised']], 401);
    }
}