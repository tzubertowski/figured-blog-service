<?php

namespace App\Posts\Inputs;

final class UpdatePostRequest
{
    public static function rules()
    {
        return [
            'title' => 'sometimes|string',
            'body' => 'sometimes|string',
            'status' => 'sometimes|in:ready,available,archived'
        ];
    }
}
