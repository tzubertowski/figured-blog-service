<?php

namespace App\Posts\Inputs;

final class StorePostRequest
{
    public static function rules()
    {
        return [
            'title' => 'required|string',
            'body' => 'required|string',
            'status' => 'required|in:ready,available,archived'
        ];
    }
}
