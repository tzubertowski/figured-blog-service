<?php

namespace App\Posts\Services;

use App\Posts\Post;
use App\Posts\PostsRepository;

final class PostService
{
    private $postsRepository;

    public function __construct(PostsRepository $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }

    public function store(array $data): Post
    {
        $post = new Post();
        $post->title = $data['title'];
        $post->body = $data['body'];
        $post->status = $data['status'];

        $post->save();
        return $post;
    }

    public function update(string $postId, array $data): Post
    {
        $post = $this->postsRepository->get($postId);
        if (array_key_exists('title', $data)) {
            $post->title = $data['title'];
        }
        if (array_key_exists('body', $data)) {
            $post->body = $data['body'];
        }
        if (array_key_exists('status', $data)) {
            $post->status = $data['status'];
        }
        $post->save();
        return $post;
    }

    public function delete(string $postId)
    {
        $post = $this->postsRepository->get($postId);
        $post->delete();
        return true;
    }
}