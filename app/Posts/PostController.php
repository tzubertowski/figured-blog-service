<?php

namespace App\Posts;

use App\Posts\Inputs\StorePostRequest;
use App\Posts\Inputs\UpdatePostRequest;
use App\Posts\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\ValidationException;
use Validator;

class PostController extends Controller
{
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index()
    {
        return Post::all();
    }

    public function store(Request $request)
    {
        $this->validate($request->all(), StorePostRequest::rules());
        return response()->json($this->postService->store($request->all()));
    }

    public function update(string $postId, Request $request)
    {
        $this->validate($request->all(), UpdatePostRequest::rules());
        return response()->json($this->postService->update($postId, $request->all()));
    }

    public function delete(string $postId, Request $request): JsonResponse
    {
        return response()->json($this->postService->delete($postId, $request->all()));
    }

    /**
     * @param array $data
     * @param array $rules
     * @throws ValidationException
     */
    private function validate(array $data, array $rules)
    {
        $validator = Validator::make($data, $rules);
        if (!$validator->passes()) {
            throw new ValidationException($validator);
        }
    }
}