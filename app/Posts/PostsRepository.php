<?php

namespace App\Posts;

final class PostsRepository
{
    public function get(string $id)
    {
        return Post::where('_id', '=', $id)->firstOrFail();
    }
}