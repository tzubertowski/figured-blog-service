## About
This repository contains RESTful API service for Figured API blog functionality.

## Dependencies
This project is based off Laravel 5.6, it's dependencies are defined in [composer.json](composer.json) file.

## API documentation
Please see the relevant [swagger docs](api.yml) for API documentation.
