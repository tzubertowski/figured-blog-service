<?php

namespace Helper;

use App\Auth\AuthorizationInterace;
use App\Auth\AuthService;
use Codeception\TestCase;
use Illuminate\Support\Facades\Schema;

class MongoDBHelper extends \Codeception\Module
{
    public function _before(TestCase $test)
    {
        $dbName = app('config')['database.default'];
        \DB::purge($dbName);
        app()->forgetInstance('db.connection.mongodb');
        $testConfig = app('config')['database.connections.mongodb'];
        $testConfig['database'] = 'test';
        config(['database.connections.mongodb' => $testConfig,]);
        \DB::reconnect($dbName);
    }

    public function _after(TestCase $test)
    {
        Schema::drop('posts');
    }

    public function amAuthorised()
    {
        $mock_resource_server = \Mockery::mock(AuthService::class)->makePartial();
        $mock_resource_server->shouldReceive('isAuthenticated')->andReturn(true);
        app()->instance(AuthorizationInterace::class, $mock_resource_server);
    }
}