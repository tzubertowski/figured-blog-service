<?php

namespace Helper;

use Codeception\Module\Laravel5;

class Laravel5Helper extends Laravel5
{
    public function _before(\Codeception\TestInterface $test)
    {
        $this->client = new \Codeception\Lib\Connector\Laravel5($this);

        // Database migrations should run before database cleanup transaction starts
        if ($this->config['run_database_migrations']) {
            $this->callArtisan('migrate', ['--path' => $this->config['database_migrations_path']]);
        }

        if ($this->config['run_database_seeder']) {
            $this->callArtisan('db:seed', ['--class' => $this->config['database_seeder_class']]);
        }
    }
}