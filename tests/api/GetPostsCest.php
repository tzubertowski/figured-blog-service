<?php


use App\Auth\AuthService;

class GetPostsCest
{
    const URL = '/posts';
    private $authService;

    public function _before(ApiTester $I)
    {
        $this->authService = \Mockery::mock(AuthService::class);
        app()->instance(AuthService::class, $this->authService);
    }

    public function _after(ApiTester $I)
    {
    }

    public function getsEmptyListsOfPosts(ApiTester $I)
    {
        $I->sendGET(self::URL);
        $response = json_decode($I->grabResponse(), true);
        $I->assertEmpty($response);
    }

    public function getsListOfPosts(ApiTester $I)
    {
        $this->producePosts($I);
        $I->sendGET(self::URL);
        $response = json_decode($I->grabResponse(), true);
        $I->assertNotEmpty($response);
    }

    private function producePosts(ApiTester $I)
    {
        $post = new App\Posts\Post();
        $post->title = 'testPost';
        $post->body = 'test description';
        $post->status = 'ready';
        $post->save();
    }
}
