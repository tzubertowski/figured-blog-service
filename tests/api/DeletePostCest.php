<?php


class DeletePostCest
{
    const URL = '/posts/%s';

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function failsToDeletePostWhenUnauthorised(ApiTester $I)
    {
        $I->sendDELETE(sprintf(self::URL, '123'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED);
    }

    public function failsToDeletePostThatDoesntExist(ApiTester $I)
    {
        $I->amAuthorised();
        $I->sendDELETE(sprintf(self::URL, 'not-a-valid-id'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
    }

    public function deletesPostWhenAuthorised(ApiTester $I)
    {
        $I->amAuthorised();
        $post = new App\Posts\Post();
        $post->save();
        $I->sendDELETE(sprintf(self::URL, $post->_id));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
    }
}
