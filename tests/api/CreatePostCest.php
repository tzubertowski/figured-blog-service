<?php


class CreatePostCest
{
    const URL = '/posts';
    private $authMiddleware;

    public function _before(ApiTester $I)
    {
        $this->authMiddleware = \Mockery::mock('overload:App\Middleware\AuthMiddleware');
    }

    public function _after(ApiTester $I)
    {
    }

    public function failsToCreatePostWithoutAuthentication(ApiTester $I)
    {
        $I->sendPOST(self::URL);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
        $I->seeResponseContains('error');
    }

    public function failsToCreatePostWithoutRequiredFields(ApiTester $I)
    {
        $I->amAuthorised();
        $this->authMiddleware->shouldReceive('handle')->andReturn();
        $I->sendPOST(self::URL, ['title' => 'one required field',]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('validation');
    }

    public function createsPostWhenAuthorised(ApiTester $I)
    {
        $I->amAuthorised();
        $I->sendPOST(
            self::URL,
            [
                'title' => 'very valid',
                'body' => 'post data',
                'status' => 'ready',
            ]
        );
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
    }
}
