<?php

use App\Posts\Post;

class UpdatePostCest
{
    const URL = '/posts/%s';

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    public function failsToUpdatePostWithoutAuthentication(ApiTester $I)
    {
        $I->sendPUT(sprintf(self::URL, '101'));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
    }

    public function failsToUpdatePostWithoutRequiredFields(ApiTester $I)
    {
        $I->amAuthorised();
        $post = new Post();
        $post->save();
        $I->sendPUT(sprintf(self::URL, $post->_id), ['status' => 'wrongstatus',]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContains('validation');
    }

    public function updatesPostWhenAuthorised(ApiTester $I)
    {
        $I->amAuthorised();
        $post = new Post();
        $post->save();
        $I->sendPUT(sprintf(self::URL, $post->_id), ['title' => 'new cool title',]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContains('new cool title');
    }
}
