<?php


use App\Auth\AuthService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class AuthServiceCest
{
    private $httpClient;
    private $request;

    public function _before(UnitTester $I)
    {
        $this->httpClient = \Mockery::mock(Client::class);
        $this->request = \Mockery::mock(Request::class);
    }

    public function _after(UnitTester $I)
    {
    }

    public function returnsFalseWhenNoHeaderPresent(UnitTester $I)
    {
        $service = $this->getInstance();
        $this->request->shouldReceive('hasHeader')->with('Authorization')->andReturn(false);
        $I->assertFalse($service->isAuthenticated($this->request));
    }

    public function returnsFalseWhenTokenValidationFails(UnitTester $I)
    {
        $service = $this->getInstance();
        // passing valid token
        $this->request->shouldReceive('hasHeader')->with('Authorization')->andReturn(true);
        $this->request->shouldReceive('header')->with('Authorization')->andReturn('imsuchatoken');
        $response = \Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getStatusCode')->andReturn(403);
        $this->httpClient->shouldReceive('request')->andReturn($response);

        // Even though the token was present Auth service did not verify it
        $I->assertFalse($service->isAuthenticated($this->request));
    }

    public function returnsTrueWhenTokenValidationSucceeds(UnitTester $I)
    {
        $service = $this->getInstance();
        // passing valid token
        $this->request->shouldReceive('hasHeader')->with('Authorization')->andReturn(true);
        $this->request->shouldReceive('header')->with('Authorization')->andReturn('imsuchatoken');
        $response = \Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getStatusCode')->andReturn(200);
        $this->httpClient->shouldReceive('request')->andReturn($response);

        // Even though the token was present Auth service did not verify it
        $I->assertTrue($service->isAuthenticated($this->request));
    }

    private function getInstance()
    {
        return new AuthService($this->httpClient);
    }
}
