<?php

use App\Auth\AuthService;
use App\Http\Middleware\AuthMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthMiddlewareCest
{
    private $authService;
    private $request;

    public function _before(UnitTester $I)
    {
        $this->authService = \Mockery::mock(AuthService::class);
        $this->request = \Mockery::mock(Request::class);
    }

    public function _after(UnitTester $I)
    {
    }

    public function testMiddlewareStopsExecutingWhenAuthServiceDoesNotAuthorise(UnitTester $I)
    {
        $this->authService->shouldReceive('isAuthenticated')->andReturn(false);
        $middleware = $this->getInstance();
        $result = $middleware->handle($this->request, function () { return false;});
        $I->assertInstanceOf(JsonResponse::class, $result);
        $I->assertContains('Unauthorised', $result->content());
    }

    public function testMiddlewareContinuesExecutingWhenAuthServiceAuthorises(UnitTester $I)
    {
        $this->authService->shouldReceive('isAuthenticated')->andReturn(true);
        $middleware = $this->getInstance();
        $hasContinued = false;
        $middleware->handle($this->request, function () use (&$hasContinued) {
            $hasContinued = true;
            return response()->json($hasContinued);
        });
        $I->assertTrue($hasContinued);
    }

    private function getInstance()
    {
        return new AuthMiddleware($this->authService);
    }
}