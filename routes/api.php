<?php

Route::get('/posts', '\App\Posts\PostController@index');
Route::middleware(['auth'])->group(function () {
    Route::post('/posts', '\App\Posts\PostController@store');
    Route::put('/posts/{id}', '\App\Posts\PostController@update');
    Route::delete('/posts/{id}', '\App\Posts\PostController@delete');
});

